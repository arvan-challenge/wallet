<?php

namespace App\Contracts;

use App\Credit;
use Pkg\Pagination\PaginationParams;

interface CreditService
{
    /**
     * Get a user credit info
     *
     * @param string $userID
     * @return Credit
     */
    public function getUserCredit(string $userID): Credit;

    /**
     * Fetch a user transactions
     *
     * @param string $userID
     * @param PaginationParams $pp
     * @return mixed
     */
    public function fetchUserTransactions(string $userID, PaginationParams $pp);

    /**
     * Apply a coupon to user wallet
     *
     * @param string $userID
     * @param string $couponCode
     * @return mixed
     */
    public function applyCoupon(string $userID, string $couponCode);
}
