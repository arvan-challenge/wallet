<?php

namespace App\Contracts;

use App\Coupon;

/**
 * Discount service
 */
interface DiscountService
{
    /**
     * Use a coupon
     *
     * @param string $code
     * @param string $userID
     * @return Coupon|null
     */
    public function useCoupon(string $couponCode, string $userID): ?Coupon;

    /**
     * Approve used coupon
     *
     * @param string $code
     * @param string $userID
     * @return bool
     */
    public function approveUsedCoupon(string $couponCode, string $userID): bool;
}
