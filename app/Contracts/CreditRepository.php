<?php

namespace App\Contracts;

use App\Credit;
use App\Transaction;
use Pkg\Pagination\PaginationParams;

/**
 * Credit repository
 */
interface CreditRepository extends TransactionalRepository
{
    /**
     * Get a user credit
     *
     * @param string $userID
     * @return Credit|null
     */
    public function getUserCredit(string $userID): ?Credit;

    /**
     * Get a user credit
     *
     * @param string $userID
     * @return Credit|null
     */
    public function getUserCreditL(string $userID): ?Credit;

    /**
     * Update a credit balance
     *
     * @param string $userID
     * @param integer $balance
     * @return Credit
     */
    public function updateCredit(string $userID, int $balance): Credit;

    /**
     * Log a credit change in transactions
     *
     * @param string $userID
     * @param integer $amount
     * @param string $type
     * @param integer $total
     * @return Transaction|null
     */
    public function log($userID, $amount, $type, $total): ?Transaction;

    /**
     * Fetch a user transactions
     *
     * @param string $userID
     * @param PaginationParams $pp
     * @return mixed
     */
    public function fetchUserTransactions(string $userID, PaginationParams $pp);
}
