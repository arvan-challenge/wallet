<?php

namespace App\Contracts;

/**
 * Transactional repository
 */
interface TransactionalRepository extends Repository {
    /**
     * Start a transaction
     *
     * @return void
     */
    public function startTransaction();

    /**
     * RollBack current transaction
     *
     * @return void
     */
    public function rollBack();

    /**
     * Commit current transaction
     *
     * @return void
     */
    public function commit();
}
