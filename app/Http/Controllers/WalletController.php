<?php

namespace App\Http\Controllers;

use App\Contracts\CreditService;
use App\Exceptions\CouponAlreadyUsedException;
use App\Exceptions\CouponIsOver;
use App\Exceptions\ExpiredCouponException;
use App\Exceptions\InvalidCouponException;
use App\Http\Requests\ApplyCoupon;
use App\Templates\ResponseTemplate;
use Illuminate\Http\Request;
use Pkg\Pagination\Pagination;

class WalletController extends Controller
{
    /**
     * @var CreditService credit service instance
     */
    protected CreditService $creditService;

    public function __construct(CreditService $creditService)
    {
        $this->creditService = $creditService;
    }

    public function applyCoupon(ApplyCoupon $req)
    {
        $userID = $req->get('user_id');
        $couponCode = $req->get('coupon_code');

        try {
            $this->creditService->applyCoupon($userID, $couponCode);

            return ResponseTemplate::Ok([
                'msg' => 'coupon applied',
            ]);
        } catch (InvalidCouponException | ExpiredCouponException $e) {
            return ResponseTemplate::BadRequest($e, 'InvalidCoupon');
        } catch (CouponAlreadyUsedException $e) {
            return ResponseTemplate::BadRequest($e, 'CouponAlreadyUsed');
        } catch (CouponIsOver $e) {
            return ResponseTemplate::BadRequest($e, 'CouponIsOver');
        } catch (\Exception $e) {
            return ResponseTemplate::InternalServerError($e);
        }
    }

    /**
     * Get user credit
     *
     * @param string $userID
     * @return void
     */
    public function getUserCredit($userID)
    {
        $credit = $this->creditService->getUserCredit($userID);

        return ResponseTemplate::Ok([
            'credit' => $credit,
        ]);
    }

    /**
     * Fetch user transactions
     *
     * @param Request $req
     * @param string $userID
     * @return void
     */
    public function fetchUserTransactions(Request $req, string $userID)
    {
        $pp = Pagination::parsePaginationParams($req);

        list($transactions, $paginationInfo) = $this->creditService->fetchUserTransactions($userID, $pp);

        return ResponseTemplate::OkWithPagination([
            'transactions' => $transactions,
        ], $paginationInfo);
    }

}
