<?php

namespace App\Services;

use App\Contracts\CreditRepository;
use App\Contracts\DiscountRepository;
use App\Credit;
use App\Contracts\CreditService as CreditServiceContract;
use App\TransactionType;
use Pkg\Pagination\PaginationParams;

class CreditService implements CreditServiceContract
{
    /**
     * Credit repository instance
     * @var CreditRepository $creditRepo
     */
    protected CreditRepository $creditRepo;

    /**
     * Discount repository instance
     * @var DiscountRepository $discountRepo
     */
    protected DiscountRepository $discountRepo;

    /**
     * Contruct a coupon service
     *
     * @param CreditRepository $repository
     */
    public function __construct(CreditRepository $creditRepo, DiscountRepository $discountRepo)
    {
        $this->creditRepo = $creditRepo;

        $this->discountRepo = $discountRepo;
    }

    /**
     * Get a user credit info
     *
     * @param string $userID
     * @return Credit
     */
    public function getUserCredit(string $userID): Credit
    {
        return $this->creditRepo->getUserCredit($userID);
    }

    /**
     * Fetch a user transactions
     *
     * @param string $userID
     * @param PaginationParams $pp
     * @return mixed
     */
    public function fetchUserTransactions(string $userID, PaginationParams $pp)
    {
        return $this->creditRepo->fetchUserTransactions($userID, $pp);
    }

    /**
     * Apply a coupon to user wallet
     *
     * @param string $userID
     * @param string $couponCode
     * @return mixed
     */
    public function applyCoupon(string $userID, string $couponCode)
    {
        $coupon = $this->discountRepo->reserveCoupon($couponCode, $userID);

        $this->creditRepo->startTransaction();

        try {
            $credit = $this->creditRepo->getUserCreditL($userID);

            // Add coupon value to current credit
            $updatedBalance = $credit->balance + $coupon->value;

            $this->creditRepo->updateCredit($userID, $updatedBalance);

            $this->creditRepo->log($userID, $coupon->value, TransactionType::CHARGE, $updatedBalance);

            $this->discountRepo->approveUsedCoupon($couponCode, $userID);

            $this->creditRepo->commit();
        } catch (\Exception $e) {
            $this->creditRepo->rollBack();

            fire('discount.couponRejected', [
                'code' => $couponCode,
                'user_id' => $userID,
            ]);

            throw $e;
        }
    }
}
