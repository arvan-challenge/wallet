<?php

namespace App\Services;

use App\Contracts\DiscountRepository;
use App\Contracts\DiscountService as DiscountServiceContract;
use App\Coupon;

/**
 * Discount service implementation
 */
class DiscountService implements DiscountServiceContract
{
    /**
     * Discount repository instance
     * @var DiscountRepository $repository
     */
    protected DiscountRepository $repository;

    /**
     * Contruct a coupon service
     *
     * @param DiscountRepository $repository
     */
    public function __construct(DiscountRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Use a coupon
     *
     * @param string $code
     * @param string $userID
     * @return Coupon|null
     */
    public function useCoupon(string $couponCode, string $userID): ?Coupon
    {
        return $this->repository->useCoupon($couponCode, $userID);
    }

    /**
     * Approve used coupon
     *
     * @param string $code
     * @param string $userID
     * @return bool
     */
    public function approveUsedCoupon(string $couponCode, string $userID): bool
    {
        return $this->repository->approveUsedCoupon($couponCode, $userID);
    }
}
