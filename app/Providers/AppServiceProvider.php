<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public $bindings = [
        \App\Contracts\DiscountRepository::class => \App\Repositories\DiscountRepository::class,
        \App\Contracts\DiscountService::class => \App\Services\DiscountService::class,
        \App\Contracts\CreditRepository::class => \App\Repositories\CreditRepository::class,
        \App\Contracts\CreditService::class => \App\Services\CreditService::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
