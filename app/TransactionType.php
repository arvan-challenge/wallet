<?php

namespace App;

class TransactionType
{
    const WITHDRAWAL = 'withdrawal';
    const CHARGE = 'charge';
    const DEPOSIT = 'deposit';
}
