<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    protected $table = "credits";

    protected $fillable = [
        'user_id',
        'balance',
        'currency',
    ];

    protected $hidden = [
        'id',
    ];
}
