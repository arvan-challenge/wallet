<?php

namespace App\Repositories;

use App\Contracts\CreditRepository as CreditRepositoryContract;
use App\Credit;
use App\Currency;
use App\Traits\HandleTransaction;
use App\Transaction;
use Pkg\Pagination\Pagination;
use Pkg\Pagination\PaginationParams;

/**
 * Credit repository
 */
class CreditRepository implements CreditRepositoryContract
{
    use HandleTransaction;

    /**
     * Get a user credit
     *
     * @param string $userID
     * @return Credit|null
     */
    public function getUserCredit(string $userID): ?Credit
    {
        return Credit::where('user_id', $userID)->firstOr(function () use ($userID) {
            $c = new Credit();

            $c->user_id = $userID;
            $c->balance = 0;
            $c->currency = Currency::IRR;

            return $c;
        });
    }

    /**
     * Get a user credit
     *
     * @param string $userID
     * @return Credit|null
     */
    public function getUserCreditL(string $userID): ?Credit
    {
        return Credit::where('user_id', $userID)
            ->lockForUpdate()
            ->firstOr(function () use ($userID) {
                $c = new Credit();

                $c->user_id = $userID;
                $c->balance = 0;
                $c->currency = Currency::IRR;

                return $c;
            });
    }

    /**
     * Update a credit balance
     *
     * @param string $userID
     * @param integer $balance
     * @return Credit
     */
    public function updateCredit(string $userID, int $balance): Credit
    {
        return Credit::updateOrCreate(
            ['user_id' => $userID],
            ['balance' => $balance],
        );
    }

    /**
     * Log a credit change in transactions
     *
     * @param string $userID
     * @param integer $amount
     * @param string $type
     * @param integer $total
     * @return Transaction|null
     */
    public function log($userID, $amount, $type, $total): ?Transaction
    {
        $tr = new Transaction();
        $tr->user_id = $userID;
        $tr->amount = $amount;
        $tr->type = $type;
        $tr->total = $total;

        $tr->save();

        return $tr;
    }

    /**
     * Fetch a user transactions
     *
     * @param string $userID
     * @param PaginationParams $pp
     * @return mixed
     */
    public function fetchUserTransactions(string $userID, PaginationParams $pp)
    {
        $total = Transaction::where('user_id', $userID)->count();

        $paginationInfo = Pagination::paginate($pp->limit, $pp->page, $total);

        $transactions = Transaction::where('user_id', $userID)
            ->limit($paginationInfo->limit)
            ->offset($paginationInfo->offset)
            ->get();

        return [$transactions, $paginationInfo];
    }
}
