<?php

namespace App\Repositories;

use App\Contracts\DiscountRepository as DiscountRepositoryContract;
use App\Coupon;
use App\Exceptions\CouponAlreadyUsedException;
use App\Exceptions\CouponIsOver;
use App\Exceptions\InvalidCouponException;
use Illuminate\Http\Client\Factory as HttpClient;
use Illuminate\Support\Facades\Http;

/**
 * Discount Repository
 */
class DiscountRepository implements DiscountRepositoryContract
{
    /**
     * http client
     * @var HttpClient $httpClient
     */
    protected HttpClient $httpClient;

    /**
     * Base url for discount service
     * @var string @baseURL
     */
    protected string $baseURL;

    /**
     * Timout for discount service
     * @var integer @timeout
     */
    protected int $timeout;

    public function __construct(HttpClient $httpClient)
    {
        $baseURL = config('services.discount.base_url', 'localhost:8000');
        $timeout = config('services.discount.timeout', 30);

        $this->baseURL = $baseURL;
        $this->timeout = $timeout;

        $this->httpClient = $httpClient;
    }

    /**
     * Join pathes
     *
     * @param string $baseURL
     * @param string[] ...$args
     * @return string
     */
    private function _basedPath(...$paths): string
    {
        return $this->baseURL . '/' . implode('/', $paths);
    }

    /**
     * Get initalized http client
     *
     * @return mixed
     */
    private function _httpClient()
    {
        return $this->httpClient
            ->timeout($this->timeout)
            ->withHeaders([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ]);
    }

    /**
     * Use a coupon
     *
     * @param string $code
     * @param string $userID
     * @return Coupon|null
     */
    public function useCoupon(string $couponCode, string $userID): ?Coupon
    {
        $useCouponPath = $this->_basedPath('coupons', 'use');

        $res = $this->_httpClient()->post($useCouponPath, [
            'coupon_code' => $couponCode,
            'user_id' => $userID,
        ]);

        if ($res->failed()) {
            if ($res->serverError() || !isset($res['code'])) {
                throw new \Exception('Discount service is not available');
            }

            $errorCode = $res['code'];
            switch ($errorCode) {
                case 'InvalidCoupon':
                    throw new InvalidCouponException('coupon is invalid');
                case 'CouponIsOver':
                    throw new CouponIsOver('coupon is over');
                case 'CouponAlreadyUsed':
                    throw new CouponAlreadyUsedException('coupon already used');
            }
        }

        $couponData = $res['data']['coupon'];

        $coupon = new Coupon();
        $coupon->code = $couponData['code'];
        $coupon->created_at = $couponData['created_at'];
        $coupon->expire = $couponData['expire'];
        $coupon->hits = $couponData['hits'];
        $coupon->id = $couponData['id'];
        $coupon->max_hits = $couponData['max_hits'];
        $coupon->status = $couponData['status'];
        $coupon->title = $couponData['title'];
        $coupon->type = $couponData['type'];
        $coupon->updated_at = $couponData['updated_at'];
        $coupon->value = $couponData['value'];


        return $coupon;
    }

    /**
     * Reserve a coupon
     *
     * @param string $code
     * @param string $userID
     * @return Coupon|null
     */
    public function reserveCoupon(string $couponCode, string $userID): ?Coupon
    {
        $useCouponPath = $this->_basedPath('coupons', 'reserve');

        $res = $this->_httpClient()->post($useCouponPath, [
            'coupon_code' => $couponCode,
            'user_id' => $userID,
        ]);

        if ($res->failed()) {
            if ($res->serverError() || !isset($res['code'])) {
                throw new \Exception('Discount service is not available');
            }

            $errorCode = $res['code'];
            switch ($errorCode) {
                case 'InvalidCoupon':
                    throw new InvalidCouponException('coupon is invalid');
                case 'CouponIsOver':
                    throw new CouponIsOver('coupon is over');
                case 'CouponAlreadyUsed':
                    throw new CouponAlreadyUsedException('coupon already used');
            }
        }

        $couponData = $res['data']['coupon'];

        $coupon = new Coupon();
        $coupon->code = $couponData['code'];
        $coupon->created_at = $couponData['created_at'];
        $coupon->expire = $couponData['expire'];
        $coupon->hits = $couponData['hits'];
        $coupon->id = $couponData['id'];
        $coupon->max_hits = $couponData['max_hits'];
        $coupon->status = $couponData['status'];
        $coupon->title = $couponData['title'];
        $coupon->type = $couponData['type'];
        $coupon->updated_at = $couponData['updated_at'];
        $coupon->value = $couponData['value'];


        return $coupon;
    }

    /**
     * Approve used coupon
     *
     * @param string $code
     * @param string $userID
     * @return bool
     */
    public function approveUsedCoupon(string $couponCode, string $userID): bool
    {
        $approveCouponPath = $this->_basedPath('coupons', 'usages', 'accept');

        $res = $this->_httpClient()->put($approveCouponPath, [
            'coupon_code' => $couponCode,
            'user_id' => $userID,
        ]);

        if ($res->failed()) {
            if ($res->serverError() || !isset($res['code'])) {
                throw new \Exception('Discount service is not available');
            }

            $errorCode = $res['code'];
            switch ($errorCode) {
                case 'InvalidCoupon':
                    throw new InvalidCouponException('coupon is invalid');
            }
        }

        return true;
    }
}
