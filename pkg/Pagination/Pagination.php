<?php

namespace Pkg\Pagination;

use Illuminate\Http\Request;

class Pagination
{
    const DEFAULT_LIMIT = 20;
    const DEFAULT_PAGE = 0;
    const MAX_LIMIT = 100;

    /**
     * Parse pagination params
     *
     * @param Request $req
     * @return PaginationParams
     */
    public static function parsePaginationParams(Request $req): PaginationParams
    {
        $limit = $req->query('limit', self::DEFAULT_LIMIT);
        $page = $req->query('page', self::DEFAULT_PAGE);

        if (!is_numeric($limit) || $limit < 0) {
            $limit = self::DEFAULT_LIMIT;
        }

        if (!is_numeric($page) || $page < 0) {
            $limit = self::DEFAULT_PAGE;
        }

        if ($limit > self::MAX_LIMIT) {
            $limit = self::MAX_LIMIT;
        }

        return new PaginationParams($limit, $page);
    }

    /**
     * Paginate
     *
     * @param integer $limit
     * @param integer $page
     * @param integer $total
     * @return PaginationInfo
     */
    public static function paginate(int $limit, int $page, int $total): PaginationInfo
    {
        $info = new PaginationInfo();

        $info->limit = $limit;
        $info->page = $page;
        $info->pages = 0;
        $info->offset = 0;
        $info->total = $total;
        $info->next = null;
        $info->prev = null;

        if ($total < 1) {
            return $info;
        }

        if ($limit > $total) {
            $info->limit = $total;
        }

        $info->pages = ceil($info->total / $info->limit);
        $info->offset = $info->page * $info->limit;

        if (($info->page + 1) < $info->pages) {
            $info->next = $info->page + 1;
        }

        if ($info->page > 0) {
            $info->prev = $info->page - 1;
        }

        return $info;
    }
}
