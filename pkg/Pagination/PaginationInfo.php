<?php

namespace Pkg\Pagination;

/**
 * Pagination info
 */
class PaginationInfo
{
    public int $limit;
    public int $page;
    public int $offset;
    public int $total;
    public int $pages;
    public ?int $next;
    public ?int $prev;
}
